import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  //Es la vista
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Bienvenido a Angular';

  curso: string = 'Curso Spring 5 con Angular 7';
  profesor: string = 'Andrés Guzmán';
  alumno: string = 'Carlos Colomer';
}

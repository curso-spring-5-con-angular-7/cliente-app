import { Injectable } from '@angular/core';
import {formatDate} from '@angular/common';
import { CLIENTES } from './clientes.json';
import { Cliente } from './cliente';
import { of, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import swal from 'sweetalert2';
import { Router } from '@angular/router';


// Para clase de servicio, represneta logica de negocio, se puede inyectar a otros conponentes
@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private urEndPoint :string = 'http://localhost:8080/api/clientes';
  private HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'})
  constructor(private http: HttpClient, private router: Router) { }

  getClientes(page: number): Observable<any> {
    //Creamos nuestro flujo Observable a partir de los objetos CLIENTES
//    return of(CLIENTES);

      return this.http.get(this.urEndPoint + '/page/' + page).pipe(
        //con : any, indicamos que la respuesta puede ser cualquier tipo de dato (json)
        tap((response: any)=> {
          console.log("ClienteService: tap 1");
          (response.content as Cliente[]).forEach(cliente =>{
            console.log(cliente.nombre);
          })
        }),
        //map: se encarga de transformar al tipo cliente
        map((response: any) => {
          //Devolvemos al map, los nombres en mayusculas y la fecha en otro orden
          (response.content as Cliente[]).map(cliente =>{
            cliente.nombre = cliente.nombre.toUpperCase();
            //Las 4s E, nos muestra el día de la semana, y las 4 M el mes en nombre (también se puede sustituir por fulldate)
            //cliente.createAt = formatDate(cliente.createAt, 'EEEE dd, MMMM yyyy', "es");
            return cliente;
          });
          return response;
        }),
        //Ahora no hace falta transformar el response, porque el map ya lo he transformado
        tap(response=> {
          console.log("ClienteService: tap 2");
          (response.content as Cliente[]).forEach(cliente =>{
            console.log(cliente.nombre);
          })
        }),
      );
   }

   create(cliente: Cliente) : Observable<Cliente>{
     //El post se utuliza para crear
     return this.http.post<Cliente>(this.urEndPoint, cliente,{headers: this.HttpHeaders}).pipe(
       //capturamos los errores
       catchError(e =>{

         if(e.status==400){
           return throwError(e);
         }

         console.error(e.error.mensaje);
         swal(e.error.mensaje, e.error.error, "error");
         return throwError(e);
       })
     );
   }

   getCliente(id): Observable<Cliente>{
     return this.http.get<Cliente>(`${this.urEndPoint}/${id}`).pipe(
       //Atrapamos el error que nos envia el servidor
       catchError(e => {
         this.router.navigate(['/clientes']);
         console.error(e.error.mensaje);
         swal("Error al editar", e.error.mensaje, "error");
         //Mostramos el error
         return throwError(e);
       })
     )
   }

   update(cliente: Cliente): Observable<Cliente>{
     //El put se utiliza para actualizar
     return this.http.put<Cliente>(`${this.urEndPoint}/${cliente.id}`, cliente, {headers: this.HttpHeaders}).pipe(
       catchError(e =>{

         if(e.status==400){
           return throwError(e);
         }

         console.error(e.error.mensaje);
         swal(e.error.mensaje, e.error.error, "error");
         return throwError(e);
       })
     );
   }

   delete(id: number): Observable<Cliente>{
     return this.http.delete<Cliente>(`${this.urEndPoint}/${id}`, {headers:this.HttpHeaders}).pipe(
       catchError(e =>{
         console.error(e.error.mensaje);
         swal(e.error.mensaje, e.error.error, "error");
         return throwError(e);
       })
     );
   }

}

import { Cliente } from './cliente';

export const CLIENTES: Cliente[] = [
  {id: 1, nombre: 'Carlos', apellido: 'Colomer', email:'carloscolomer99@gmail.com', createAt:'12/12/2020'},
  {id: 2, nombre: 'Raul', apellido: 'Doe', email:'rauldoe@gmail.com', createAt:'12/12/2020'},
  {id: 3, nombre: 'Richard', apellido: 'Revert', email:'richardrevert@gmail.com', createAt:'12/12/2020'},
  {id: 4, nombre: 'Juan', apellido: 'Gisbert', email:'juangisbert@gmail.com', createAt:'12/12/2020'},
  {id: 5, nombre: 'Antonio', apellido: 'Molla', email:'antoniomolla@gmail.com', createAt:'12/12/2020'},
  {id: 6, nombre: 'Maria', apellido: 'Perez', email:'mariaperez@gmail.com', createAt:'12/12/2020'}

];

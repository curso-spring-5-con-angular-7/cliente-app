
import { Component } from '@angular/core';
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {
 // Tipo any es generico
  public autor: any = {nombre: 'Carlos', apellido: 'Colomer'};


}
